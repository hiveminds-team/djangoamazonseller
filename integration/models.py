from _decimal import Decimal

from django.db import models


# Create your models here.
class AmsAll(models.Model):
    campaignId = models.CharField(max_length=100, db_column='campaignId')
    profileId = models.CharField(max_length=100, db_column='profileId')
    recordType = models.CharField(max_length=100, db_column='recordType')
    campaignName = models.CharField(max_length=100, db_column='campaignName')
    reportType = models.CharField(max_length=100, db_column='reportType')
    campaignStatus = models.CharField(max_length=100, db_column='campaignStatus')
    clicks = models.BigIntegerField(db_column='clicks')
    attributedUnitsSold14d = models.BigIntegerField(db_column='attributedUnitsSold14d')
    cost = models.DecimalField(max_digits=20, decimal_places=4, default=Decimal(0.0000), db_column='cost')
    attributedDPV14d = models.BigIntegerField(db_column='attributedDPV14d')
    currency = models.CharField(max_length=100, db_column='currency')
    date = models.DateField(db_column='date')
    CustomerName = models.CharField(max_length=100, db_column='CustomerName')
    attributedSales14d = models.DecimalField(max_digits=20, decimal_places=4, default=Decimal(0.0000),
                                             db_column='attributedSales14d')
    impressions = models.BigIntegerField(db_column='impressions')
    campaignBudget = models.DecimalField(max_digits=20, decimal_places=4, default=Decimal(0.0000),
                                         db_column='campaignBudget')
    attributedConversions14d = models.DecimalField(max_digits=20, decimal_places=4, default=Decimal(0.0000),
                                                   db_column='attributedConversions14d')
    attributedConversions14dSameSKU = models.DecimalField(max_digits=20, decimal_places=4, default=Decimal(0.0000),
                                                          db_column='attributedConversions14dSameSKU')
    attributedSales14dSameSKU = models.DecimalField(max_digits=20, decimal_places=4, default=Decimal(0.0000),
                                                    db_column='attributedSales14dSameSKU')
    attributedUnitsOrdered14dSameSKU = models.DecimalField(max_digits=20, decimal_places=4, default=Decimal(0.0000),
                                                           db_column='attributedUnitsOrdered14dSameSKU')
    attributedConversions1d = models.DecimalField(max_digits=20, decimal_places=4, default=Decimal(0.0000),
                                                  db_column='attributedConversions1d')
    attributedConversions7d = models.DecimalField(max_digits=20, decimal_places=4, default=Decimal(0.0000),
                                                  db_column='attributedConversions7d')
    attributedConversions30d = models.DecimalField(max_digits=20, decimal_places=4, default=Decimal(0.0000),
                                                   db_column='attributedConversions30d')
    attributedConversions1dSameSKU = models.DecimalField(max_digits=20, decimal_places=4, default=Decimal(0.0000),
                                                         db_column='attributedConversions1dSameSKU')
    attributedConversions7dSameSKU = models.DecimalField(max_digits=20, decimal_places=4, default=Decimal(0.0000),
                                                         db_column='attributedConversions7dSameSKU')
    attributedConversions30dSameSKU = models.DecimalField(max_digits=20, decimal_places=4, default=Decimal(0.0000),
                                                          db_column='attributedConversions30dSameSKU')
    attributedUnitsOrdered1d = models.DecimalField(max_digits=20, decimal_places=4, default=Decimal(0.0000),
                                                   db_column='attributedUnitsOrdered1d')
    attributedUnitsOrdered7d = models.DecimalField(max_digits=20, decimal_places=4, default=Decimal(0.0000),
                                                   db_column='attributedUnitsOrdered7d')
    attributedUnitsOrdered30d = models.DecimalField(max_digits=20, decimal_places=4, default=Decimal(0.0000),
                                                    db_column='attributedUnitsOrdered30d')
    attributedSales1d = models.DecimalField(max_digits=20, decimal_places=4, default=Decimal(0.0000),
                                            db_column='attributedSales1d')
    attributedSales7d = models.DecimalField(max_digits=20, decimal_places=4, default=Decimal(0.0000),
                                            db_column='attributedSales7d')
    attributedSales30d = models.DecimalField(max_digits=20, decimal_places=4, default=Decimal(0.0000),
                                             db_column='attributedSales30d')
    attributedSales1dSameSKU = models.DecimalField(max_digits=20, decimal_places=4, default=Decimal(0.0000),
                                                   db_column='attributedSales1dSameSKU')
    attributedSales7dSameSKU = models.DecimalField(max_digits=20, decimal_places=4, default=Decimal(0.0000),
                                                   db_column='attributedSales7dSameSKU')
    attributedSales30dSameSKU = models.DecimalField(max_digits=20, decimal_places=4, default=Decimal(0.0000),
                                                    db_column='attributedSales30dSameSKU')
    attributedUnitsOrdered1dSameSKU = models.DecimalField(max_digits=20, decimal_places=4, default=Decimal(0.0000),
                                                          db_column='attributedUnitsOrdered1dSameSKU')
    attributedUnitsOrdered7dSameSKU = models.DecimalField(max_digits=20, decimal_places=4, default=Decimal(0.0000),
                                                          db_column='attributedUnitsOrdered7dSameSKU')
    attributedUnitsOrdered30dSameSKU = models.DecimalField(max_digits=20, decimal_places=4, default=Decimal(0.0000),
                                                           db_column='attributedUnitsOrdered30dSameSKU')

    class Meta:
        db_table = "ams_all"
