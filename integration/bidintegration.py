import sponsoredproduct.campaignservice as sp_campaign_service
import sponsoredproduct.adgroupservice as sp_adgroup_service
import sponsoredproduct.biddingservice as sp_bidding_service
import sponsoredproduct.keywordservice as sp_keyword_service
import pandas as pd

class BidIntegration:
    sp_campaign_service = sp_campaign_service.CampaignService()
    sp_adgroup_service = sp_adgroup_service.AdgroupService()
    sp_bidding_service = sp_bidding_service.BiddingService()
    sp_keyword_service = sp_keyword_service.KeywordService()

    def save_bid_DB(self):
        bid_data = self.get_all_bidding_data_by_profile('3357819201937200')


    def get_all_bidding_data_by_profile(self, profile_id):

        adgroup_id_list = []
        adgroup_id_map = {}
        adgroup_campaign_map = {}
        campaign_list = self.sp_campaign_service.list_campaigns(profile_id)
        for campaigns in campaign_list:
            adgroup_list = self.sp_adgroup_service.list_adgroups(profile_id, campaigns['campaignId'])
            for adgroups in adgroup_list:
                adgroup_id_list.append(adgroups['adGroupId'])
                adgroup_id_map[adgroups['adGroupId']] = adgroups['name']
                adgroup_campaign_map[adgroups['adGroupId']] = campaigns['name']
        final_bid_data = []
        for adgroup in adgroup_id_list:
            current_bid_data = self.bidding_data_by_adgroup(profile_id, adgroup)
            if len(current_bid_data) != 0:
                for bid_data in current_bid_data:
                    bid_data['adGroup'] = adgroup_id_map[adgroup]
                    bid_data['campaign'] = adgroup_campaign_map[adgroup]
                    final_bid_data.append(bid_data)
        return final_bid_data

    def bidding_data_by_adgroup(self, profile_id, adgroup_id):
        bidding_data_for_keyword = self.sp_bidding_service.get_bid_keyword_adgroup_filter(profile_id, adgroup_id)

        keywords_suggested_bid = bidding_data_for_keyword['keywords']
        keywords_suggested_bid_update = []
        current_bid = self.sp_keyword_service.list_biddable_keyword_on_filter(profile_id, adgroup_id)
        for keyword_bid in keywords_suggested_bid:
            current_bid_check = False
            for bid in current_bid:
                suggested_keyword_text = keyword_bid['keyword']
                current_keyword_text = bid['keywordText']
                suggested_keyword_match_type = keyword_bid['matchType']
                current_keyword_match_type = bid['matchType']
                if suggested_keyword_text == current_keyword_text and suggested_keyword_match_type == current_keyword_match_type:
                    if 'bid' in bid:
                        keyword_bid['currentBid'] = bid['bid']
                        keywords_suggested_bid_update.append(keyword_bid)
                        current_bid_check = True
            if not current_bid_check:
                keyword_bid['currentBid'] = 'not_available'
                keywords_suggested_bid_update.append(keyword_bid)
        bidding_data_for_keyword['keyword'] = keywords_suggested_bid_update

        keyword_data = bidding_data_for_keyword['keywords']
        for keyword in keyword_data:
            if keyword['code'] == 'NOT_FOUND':
                custom_bid_data = {'rangeEnd': '0.0', 'rangeStart': '0.0', 'suggested': '0.0'}
                keyword['suggestedBid'] = custom_bid_data

        return keyword_data
