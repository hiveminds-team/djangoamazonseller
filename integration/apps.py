from django.apps import AppConfig


class IntegrationConfig(AppConfig):
    name = 'integration'

    # def ready(self):
    #     from integration import reportschedular
    #     reportschedular.report_generate_automation()