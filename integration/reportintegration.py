import json
import time
import sponsoredproduct.reportservice as sp_report_service
import sponsoredbrand.reportservice as sb_report_service
import sponsoreddisplay.reportservice as sd_report_service
from integration.models import AmsAll
from datetime import datetime, date, timedelta
import pandas as pd
from django.core import serializers
from sqlalchemy import create_engine


class ReportIntegration:
    sp_report_service = sp_report_service.ReportService()
    sb_report_service = sb_report_service.ReportService()
    sd_report_service = sd_report_service.ReportService()

    def csv_report(self):

        for day in range(31):
            end_date = date.today() - timedelta(9)
            start_date = end_date - timedelta(day)
            date_string = start_date.strftime("%Y%m%d")
            print(date_string)
            ams_list = []

            a1 = self.save_reports_db('3357819201937200', 'campaigns', date_string, None, 'Sponsored Product',
                                      'Pidilite Industries Limited')
            a2 = self.save_reports_db('3357819201937200', 'campaigns', date_string, 'T00001', 'Sponsored Display',
                                      'Pidilite Industries Limited')
            a3 = self.save_reports_db('3357819201937200', 'campaigns', date_string, None, 'Sponsored Brand',
                                      'Pidilite Industries Limited')

            df = pd.DataFrame()

            ams_list.extend(a1)
            ams_list.extend(a2)
            ams_list.extend(a3)
            for a in ams_list:
                serialized_obj1 = serializers.serialize('json', [a, ])
                x = json.loads(serialized_obj1)
                x1 = x[0]['fields']
                df = df.append(x1, ignore_index=True)

            engine = create_engine("postgresql://{user}:{pw}@{host}/{db}"
                                   .format(user="hiveminds",
                                           pw="tr0ub4dor&3",
                                           host="45.33.19.66",
                                           db='pidellite',
                                           port=5432))
            # conn = engine.raw_connection()
            df.to_sql('ams_all', engine, if_exists='append', index=False)
            print('OK')

    def save_reports_db(self, profile_id, record_type, report_date, tactic, report_type, customer_name):
        ams_all_list = []
        if report_type == 'Sponsored Product':
            sp_report_today_all = self.generate_sp_report(record_type, profile_id, report_date)
            for sp_report_today in sp_report_today_all:
                ams_all = AmsAll(campaignId=sp_report_today['campaignId'], profileId=profile_id,
                                 recordType='campaigns', campaignName=sp_report_today['campaignName'],
                                 reportType='Sponsored Product', campaignStatus=sp_report_today['campaignStatus'],
                                 clicks=sp_report_today['clicks'],
                                 attributedUnitsSold14d=sp_report_today['attributedUnitsOrdered14d'],
                                 cost=sp_report_today['cost'], attributedDPV14d=0.0000, currency='NA',
                                 date=datetime.strptime(report_date, '%Y%m%d'),
                                 CustomerName=customer_name, attributedSales14d=sp_report_today['attributedSales14d'],
                                 impressions=sp_report_today['impressions'],
                                 campaignBudget=sp_report_today['campaignBudget'],
                                 attributedConversions14d=sp_report_today['attributedConversions14d'],
                                 attributedConversions14dSameSKU=sp_report_today['attributedConversions14dSameSKU'],
                                 attributedSales14dSameSKU=sp_report_today['attributedSales14dSameSKU'],
                                 attributedUnitsOrdered14dSameSKU=sp_report_today['attributedUnitsOrdered14dSameSKU'],
                                 attributedConversions1d=sp_report_today['attributedConversions1d'],
                                 attributedConversions7d=sp_report_today['attributedConversions7d'],
                                 attributedConversions30d=sp_report_today['attributedConversions30d'],
                                 attributedConversions1dSameSKU=sp_report_today['attributedConversions1dSameSKU'],
                                 attributedConversions7dSameSKU=sp_report_today['attributedConversions7dSameSKU'],
                                 attributedConversions30dSameSKU=sp_report_today['attributedConversions30dSameSKU'],
                                 attributedUnitsOrdered1d=sp_report_today['attributedUnitsOrdered1d'],
                                 attributedUnitsOrdered7d=sp_report_today['attributedUnitsOrdered7d'],
                                 attributedUnitsOrdered30d=sp_report_today['attributedUnitsOrdered30d'],
                                 attributedSales1d=sp_report_today['attributedSales1d'],
                                 attributedSales7d=sp_report_today['attributedSales7d'],
                                 attributedSales30d=sp_report_today['attributedSales30d'],
                                 attributedSales1dSameSKU=sp_report_today['attributedSales1dSameSKU'],
                                 attributedSales7dSameSKU=sp_report_today['attributedSales7dSameSKU'],
                                 attributedSales30dSameSKU=sp_report_today['attributedSales30dSameSKU'],
                                 attributedUnitsOrdered1dSameSKU=sp_report_today['attributedUnitsOrdered1dSameSKU'],
                                 attributedUnitsOrdered7dSameSKU=sp_report_today['attributedUnitsOrdered7dSameSKU'],
                                 attributedUnitsOrdered30dSameSKU=sp_report_today['attributedUnitsOrdered30dSameSKU'])
                ams_all_list.append(ams_all)
        if report_type == 'Sponsored Display':
            sd_report_today_all = self.generate_sd_report(record_type, profile_id, report_date, tactic)
            for sd_report_today in sd_report_today_all:
                ams_all = AmsAll(campaignId=sd_report_today['campaignId'], profileId=profile_id,
                                 recordType=record_type, campaignName=sd_report_today['campaignName'],
                                 reportType=report_type, campaignStatus=sd_report_today['campaignStatus'],
                                 clicks=sd_report_today['clicks'],
                                 attributedUnitsSold14d=sd_report_today['attributedUnitsSold14d'],
                                 cost=sd_report_today['cost'], attributedDPV14d=0.0000, currency='NA',
                                 date=datetime.strptime(report_date, '%Y%m%d'),
                                 CustomerName=customer_name, attributedSales14d=sd_report_today['attributedSales14d'],
                                 impressions=sd_report_today['impressions'], campaignBudget=0.0000,
                                 attributedConversions14d=0.0000,
                                 attributedConversions14dSameSKU=0.0000, attributedSales14dSameSKU=0.0000,
                                 attributedUnitsOrdered14dSameSKU=0.0000,
                                 attributedConversions1d=0.0000, attributedConversions7d=0.0000,
                                 attributedConversions30d=0.0000, attributedConversions1dSameSKU=0.0000,
                                 attributedConversions7dSameSKU=0.0000,
                                 attributedConversions30dSameSKU=0.0000, attributedUnitsOrdered1d=0.0000,
                                 attributedUnitsOrdered7d=0.0000, attributedUnitsOrdered30d=0.0000,
                                 attributedSales1d=0.0000, attributedSales7d=0.0000, attributedSales30d=0.0000,
                                 attributedSales1dSameSKU=0.0000, attributedSales7dSameSKU=0.0000,
                                 attributedSales30dSameSKU=0.0000,
                                 attributedUnitsOrdered1dSameSKU=0.0000, attributedUnitsOrdered7dSameSKU=0.0000,
                                 attributedUnitsOrdered30dSameSKU=0.0000)
                ams_all_list.append(ams_all)

        if report_type == 'Sponsored Brand':
            sb_report_today_all = self.generate_sb_report(record_type, profile_id, report_date)
            for sb_report_today in sb_report_today_all:
                ams_all = AmsAll(campaignId=sb_report_today['campaignId'], profileId=profile_id,
                                 recordType=record_type, campaignName=sb_report_today['campaignName'],
                                 reportType=report_type, campaignStatus=sb_report_today['campaignStatus'],
                                 clicks=sb_report_today['clicks'],
                                 attributedUnitsSold14d=sb_report_today['unitsSold14d'],
                                 cost=sb_report_today['cost'], attributedDPV14d=sb_report_today['dpv14d'],
                                 currency='NA',
                                 date=datetime.strptime(report_date, '%Y%m%d'),
                                 CustomerName=customer_name, attributedSales14d=sb_report_today['attributedSales14d'],
                                 impressions=sb_report_today['impressions'],
                                 campaignBudget=0.0000,
                                 attributedConversions14d=sb_report_today['attributedConversions14d'],
                                 attributedConversions14dSameSKU=sb_report_today['attributedConversions14dSameSKU'],
                                 attributedSales14dSameSKU=sb_report_today['attributedSales14dSameSKU'],
                                 attributedUnitsOrdered14dSameSKU=0.0000, attributedConversions1d=0.0000,
                                 attributedConversions7d=0.0000,
                                 attributedConversions30d=0.0000, attributedConversions1dSameSKU=0.0000,
                                 attributedConversions7dSameSKU=0.0000,
                                 attributedConversions30dSameSKU=0.0000, attributedUnitsOrdered1d=0.0000,
                                 attributedUnitsOrdered7d=0.0000, attributedUnitsOrdered30d=0.0000,
                                 attributedSales1d=0.0000, attributedSales7d=0.0000, attributedSales30d=0.0000,
                                 attributedSales1dSameSKU=0.0000, attributedSales7dSameSKU=0.0000,
                                 attributedSales30dSameSKU=0.0000,
                                 attributedUnitsOrdered1dSameSKU=0.0000, attributedUnitsOrdered7dSameSKU=0.0000,
                                 attributedUnitsOrdered30dSameSKU=0.0000)
                ams_all_list.append(ams_all)
        return ams_all_list
        # AmsAll.objects.bulk_create(ams_all_list)

    def generate_sp_report(self, record_type, profile_id, report_date):
        report_id = self.sp_report_service.generate_report_id(record_type, profile_id, report_date)
        report_link_response = self.sp_report_service.get_report_link(profile_id, report_id)
        report_status = report_link_response['status']
        while report_status == 'IN_PROGRESS':
            print(report_status)
            print('SP',report_link_response)
            time.sleep(5)
            report_link_response = self.sp_report_service.get_report_link(profile_id, report_id)
            report_status = report_link_response['status']
        location = report_link_response['location']
        redirect_response = self.sp_report_service.get_report_data(profile_id, location)
        return redirect_response

    def generate_sb_report(self, record_type, profile_id, report_date):
        report_id = self.sb_report_service.generate_report_id(record_type, profile_id, report_date)
        report_link = self.sb_report_service.get_report_link(profile_id, report_id)
        report_status = report_link['status']
        while report_status == 'IN_PROGRESS':
            print(report_status)
            print('SB',report_link)
            time.sleep(5)
            report_link = self.sb_report_service.get_report_link(profile_id, report_id)
            report_status = report_link['status']
        location = report_link['location']
        redirect_response = self.sb_report_service.get_report_data(profile_id, location)
        return redirect_response

    def generate_sd_report(self, record_type, profile_id, report_date, tactic):
        report_id = self.sd_report_service.generate_report_id(record_type, profile_id, report_date, tactic)
        report_link = self.sd_report_service.get_report_link(profile_id, report_id)
        report_status = report_link['status']
        while report_status == 'IN_PROGRESS':
            print(report_status)
            print('SD',report_link)
            time.sleep(5)
            report_link = self.sd_report_service.get_report_link(profile_id, report_id)
            report_status = report_link['status']
        location = report_link['location']
        redirect_response = self.sd_report_service.get_report_data(profile_id, location)
        return redirect_response
