from apscheduler.schedulers.background import BackgroundScheduler
from apscheduler.triggers.interval import IntervalTrigger
import integration.reportintegration as report_integration


def get_report():
    reports = report_integration.ReportIntegration()
    print('Generating report')

    reports.save_reports_db('3357819201937200', 'campaigns', '20201120', None, 'Sponsored Product',
                            'Pidilite Industries Limited')
    reports.save_reports_db('3357819201937200', 'campaigns', '20201120', 'T00001', 'Sponsored Display',
                            'Pidilite Industries Limited')
    reports.save_reports_db('3357819201937200', 'campaigns', '20201120', None, 'Sponsored Brand',
                            'Pidilite Industries Limited')


def report_generate_automation():
    scheduler = BackgroundScheduler()
    scheduler.add_job(get_report, 'interval', minutes=2)
    scheduler.start()