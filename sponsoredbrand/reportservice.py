import gzip

import requests
from json import loads
import authorization.authorizationservice as utils


class ReportService:
    utilities = utils.AuthorizationService()

    def generate_report_id(self, record_type, profile_id, report_date):
        report_id_url = 'https://advertising-api-eu.amazon.com/v2/hsa/' + record_type + '/report'
        request_body = {
            'reportDate': report_date,
            'metrics': 'campaignName,campaignId,campaignStatus,campaignBudget,campaignBudgetType,impressions,clicks,cost,attributedSales14d,attributedSales14dSameSKU,attributedConversions14d,attributedConversions14dSameSKU,unitsSold14d,dpv14d'
        }

        report_id = self.utilities.ams_post(report_id_url, profile_id, request_body)
        return report_id.json()['reportId']

    def get_report_link(self, profile_id, report_id):
        report_link_url = 'https://advertising-api-eu.amazon.com/v2/reports/' + report_id
        report_link = self.utilities.ams_get(report_link_url, profile_id)
        return report_link

    def get_report_data(self, profile_id, location):
        access_token = self.utilities.generate_access_token()
        requests_data = {
            'Amazon-Advertising-API-Scope': profile_id,
            'Authorization': 'Bearer ' + access_token,
            'Content-Type': 'application/json',
            'Amazon-Advertising-API-ClientId': 'amzn1.application-oa2-client.a7d9e7fefb6a406eaf15115bc98c18f5',
        }
        response = requests.get(location, headers=requests_data, allow_redirects=True)
        json_response = loads(gzip.decompress(requests.get(response.url).content))
        return json_response
