import authorization.authorizationservice as util


class CampaignService:
    utilities = util.AuthorizationService()

    def list_campaigns(self, profile_id):
        list_campaign_url = 'https://advertising-api-eu.amazon.com/sb/campaigns'
        return self.utilities.ams_get(list_campaign_url, profile_id)

    def get_campaigns(self, profile_id, campaign_id):
        campaign_get_url = 'https://advertising-api-eu.amazon.com/sb/campaigns'+campaign_id
        return self.utilities.ams_get(campaign_get_url, profile_id)

    def get_campaign_by_profile(self, profile_id, profile_name):
        profile_level_campaign_extended = []
        all_campaigns = self.list_campaigns(profile_id)
        for campaigns in all_campaigns:
            campaign_extended = self.get_campaigns(profile_id, campaigns.json()['campaignId'])
            campaign_extended_json = campaign_extended.json()
            if 'startDate' in campaign_extended_json:
                del campaign_extended_json['startDate']
            if 'endDate' in campaign_extended_json:
                del campaign_extended_json['endDate']
            if 'brandEntityId' in campaign_extended_json:
                del campaign_extended_json['brandEntityId']
            if 'budgetType' in campaign_extended_json:
                del campaign_extended_json['budgetType']
            if 'portfolioId' in campaign_extended_json:
                del campaign_extended_json['portfolioId']
            if 'landingPage' in campaign_extended_json:
                del campaign_extended_json['landingPage']
            if 'bidOptimization' in campaign_extended_json:
                del campaign_extended_json['bidOptimization']
            if 'bidMultiplier' in campaign_extended_json:
                del campaign_extended_json['bidMultiplier']
            if 'adFormat' in campaign_extended_json:
                del campaign_extended_json['adFormat']
            if 'creative' in campaign_extended_json:
                del campaign_extended_json['creative']
            campaign_extended_json['profile'] = profile_name
            campaign_extended_json['campaignType'] = 'Sponsored Brand'
            campaign_extended_json['targetingType'] = 'NA'
            campaign_extended_json['premiumBidAdjustment'] = False
            campaign_extended_json['dailyBudget'] = campaign_extended_json['budget']
            del campaign_extended_json['budget']
            profile_level_campaign_extended.append(campaign_extended_json)
        return profile_level_campaign_extended