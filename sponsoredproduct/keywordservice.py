import authorization.authorizationservice as authservice


class KeywordService:
    authservice = authservice.AuthorizationService()

    def list_biddable_keyword_on_filter(self, profile_id, adgroup_id_filter):
        list_keyword_url = 'https://advertising-api-eu.amazon.com/v2/sp/keywords/?adGroupIdFilter=' + str(
            adgroup_id_filter)
        return self.authservice.ams_get(list_keyword_url, profile_id)
