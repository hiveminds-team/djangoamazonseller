import sponsoredproduct.reportservice as reportservice
import time


class ReportHelper:
    rep = reportservice.ReportService()

    def generate_sp_report(self):
        report_id = self.rep.generate_report_id('campaigns', '4425353684214266', '20201115')
        report_link_response = self.rep.get_report_link('4425353684214266', report_id)
        report_status = report_link_response.json()['status']
        while report_status == 'IN_PROGRESS':
            print(report_status)
            time.sleep(3)
            report_link_response = self.rep.get_report_link('4425353684214266', report_id)
            report_status = report_link_response.json()['status']
        location = report_link_response.json()['location']
        redirect_response = self.rep.get_report_data('4425353684214266',location)
        return redirect_response
