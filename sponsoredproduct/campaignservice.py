from authorization import authorizationservice as util


class CampaignService:
    utilities = util.AuthorizationService()

    def list_campaigns(self, profile_id):
        list_campaign_url = 'https://advertising-api-eu.amazon.com/v2/sp/campaigns'
        return self.utilities.ams_get(list_campaign_url, profile_id)

    def get_campaign(self, profile_id, campaign_id):
        campaign_get_url = 'https://advertising-api-eu.amazon.com/v2/sp/campaigns' + campaign_id
        return self.utilities.ams_get(campaign_get_url, profile_id)

    def get_campaign_extended(self, profile_id, campaign_id):
        campaign_extended_url = 'https://advertising-api-eu.amazon.com/v2/sp/campaigns/extended' + campaign_id
        return self.utilities.ams_get(campaign_extended_url, profile_id)

    def get_campaign_by_profile(self, profile_id, profile_name):
        profile_level_campaign_extended = []
        all_campaigns = self.list_campaigns(profile_id)
        for campaigns in all_campaigns:
            campaign_extended = self.get_campaign_extended(profile_id, campaigns.json()['campaignId'])
            campaign_extended_json = campaign_extended.json()
            if 'startDate' in campaign_extended_json:
                del campaign_extended_json['startDate']
            if 'endDate' in campaign_extended_json:
                del campaign_extended_json['endDate']
            if 'portfolioId' in campaign_extended_json:
                del campaign_extended_json['portfolioId']
            campaign_extended_json['profile'] = profile_name
            del campaign_extended_json['creationDate']
            del campaign_extended_json['lastUpdatedDate']
            profile_level_campaign_extended.append(campaign_extended_json)
        return profile_level_campaign_extended