import gzip

import requests
from json import loads
import authorization.authorizationservice as utils


class ReportService:
    utilities = utils.AuthorizationService()

    def generate_report_id(self, record_type, profile_id, report_date):
        report_id_url = 'https://advertising-api-eu.amazon.com/v2/sp/' + record_type + '/report'
        request_body = {
            'reportDate': report_date
        }
        if record_type == 'keywords':
            request_body[
                'metrics'] = 'campaignName,campaignId,adGroupName,adGroupId,keywordId,keywordText,matchType,impressions,clicks,cost,attributedConversions1d,attributedConversions7d,attributedConversions14d,attributedConversions30d,attributedConversions1dSameSKU,attributedConversions7dSameSKU,attributedConversions14dSameSKU,attributedConversions30dSameSKU,attributedUnitsOrdered1d,attributedUnitsOrdered7d,attributedUnitsOrdered14d,attributedUnitsOrdered30d,attributedSales1d,attributedSales7d,attributedSales14d,attributedSales30d,attributedSales1dSameSKU,attributedSales7dSameSKU,attributedSales14dSameSKU,attributedSales30dSameSKU,attributedUnitsOrdered1dSameSKU,attributedUnitsOrdered7dSameSKU,attributedUnitsOrdered14dSameSKU,attributedUnitsOrdered30dSameSKU'
        if record_type == 'campaigns':
            request_body[
                'metrics'] = 'campaignName,campaignId,campaignStatus,campaignBudget,impressions,clicks,cost,attributedConversions1d,attributedConversions7d,attributedConversions14d,attributedConversions30d,attributedConversions1dSameSKU,attributedConversions7dSameSKU,attributedConversions14dSameSKU,attributedConversions30dSameSKU,attributedUnitsOrdered1d,attributedUnitsOrdered7d,attributedUnitsOrdered14d,attributedUnitsOrdered30d,attributedSales1d,attributedSales7d,attributedSales14d,attributedSales30d,attributedSales1dSameSKU,attributedSales7dSameSKU,attributedSales14dSameSKU,attributedSales30dSameSKU,attributedUnitsOrdered1dSameSKU,attributedUnitsOrdered7dSameSKU,attributedUnitsOrdered14dSameSKU,attributedUnitsOrdered30dSameSKU'
        if record_type == 'adGroups':
            request_body[
                'metrics'] = 'campaignName,campaignId,adGroupName,adGroupId,impressions,clicks,cost,attributedConversions1d,attributedConversions7d,attributedConversions14d,attributedConversions30d,attributedConversions1dSameSKU,attributedConversions7dSameSKU,attributedConversions14dSameSKU,attributedConversions30dSameSKU,attributedUnitsOrdered1d,attributedUnitsOrdered7d,attributedUnitsOrdered14d,attributedUnitsOrdered30d,attributedSales1d,attributedSales7d,attributedSales14d,attributedSales30d,attributedSales1dSameSKU,attributedSales7dSameSKU,attributedSales14dSameSKU,attributedSales30dSameSKU,attributedUnitsOrdered1dSameSKU,attributedUnitsOrdered7dSameSKU,attributedUnitsOrdered14dSameSKU,attributedUnitsOrdered30dSameSKU'
        if record_type == 'productAds':
            request_body[
                'metrics'] = 'campaignName,campaignId,adGroupName,adGroupId,impressions,clicks,cost,currency,asin,attributedConversions1d,attributedConversions7d,attributedConversions14d,attributedConversions30d,attributedConversions1dSameSKU,attributedConversions7dSameSKU,attributedConversions14dSameSKU,attributedConversions30dSameSKU,attributedUnitsOrdered1d,attributedUnitsOrdered7d,attributedUnitsOrdered14d,attributedUnitsOrdered30d,attributedSales1d,attributedSales7d,attributedSales14d,attributedSales30d,attributedSales1dSameSKU,attributedSales7dSameSKU,attributedSales14dSameSKU,attributedSales30dSameSKU,attributedUnitsOrdered1dSameSKU,attributedUnitsOrdered7dSameSKU,attributedUnitsOrdered14dSameSKU,attributedUnitsOrdered30dSameSKU'
        if record_type == 'asins':
            request_body[
                'metrics'] = 'campaignName,campaignId,adGroupName,adGroupId,keywordId,keywordText,asin,otherAsin,sku,currency,matchType,attributedUnitsOrdered1d,attributedUnitsOrdered7d,attributedUnitsOrdered14d,attributedUnitsOrdered30d,attributedUnitsOrdered1dOtherSKU,attributedUnitsOrdered7dOtherSKU,attributedUnitsOrdered14dOtherSKU,attributedUnitsOrdered30dOtherSKU,attributedSales1dOtherSKU,attributedSales7dOtherSKU,attributedSales14dOtherSKU,attributedSales30dOtherSKU'

        if 'metrics' not in request_body.keys():
            return 'Invalid Request(Provide correct record type)'

        report_id = self.utilities.ams_post(report_id_url, profile_id, request_body)
        return report_id.json()['reportId']

    def get_report_link(self, profile_id, report_id):
        report_link_url = 'https://advertising-api-eu.amazon.com/v2/sp/reports/' + report_id
        report_link = self.utilities.ams_get(report_link_url, profile_id)
        return report_link

    def get_report_data(self, profile_id, location):
        access_token = self.utilities.generate_access_token()
        requests_data = {
            'Amazon-Advertising-API-Scope': profile_id,
            'Authorization': 'Bearer ' + access_token,
            'Content-Type': 'application/json',
            'Amazon-Advertising-API-ClientId': 'amzn1.application-oa2-client.a7d9e7fefb6a406eaf15115bc98c18f5',
        }
        response = requests.get(location, headers=requests_data, allow_redirects=True)
        json_response = loads(gzip.decompress(requests.get(response.url).content))
        return json_response
