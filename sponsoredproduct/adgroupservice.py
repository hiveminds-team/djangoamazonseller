import authorization.authorizationservice as auth_service


class AdgroupService:
    auth = auth_service.AuthorizationService()

    def list_adgroups(self, profile_id, adgroup_filter):
        list_adgroup_url = 'https://advertising-api-eu.amazon.com/v2/sp/adGroups/?campaignIdFilter=' + str(adgroup_filter)
        return self.auth.ams_get(list_adgroup_url, profile_id)

    def get_adgroup(self,profile_id,adgroup_id):
        get_adgroup_url = 'https://advertising-api-eu.amazon.com/v2/sp/adGroups/'+str(adgroup_id)
        return self.auth.ams_get(get_adgroup_url,profile_id)

