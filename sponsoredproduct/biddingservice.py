import sponsoredproduct.keywordservice as sp_keyword_service
import authorization.authorizationservice as authorization


class BiddingService:
    sp_keyword_service = sp_keyword_service.KeywordService()
    auth = authorization.AuthorizationService()

    def get_bid_keyword_adgroup_filter(self, profile_id, adgroup_id):
        list_biddable_keyword = self.sp_keyword_service.list_biddable_keyword_on_filter(profile_id, adgroup_id)
        keyword_stack = []
        for bid_keyword in list_biddable_keyword:
            filter_data = {'keyword': bid_keyword['keywordText'], 'matchType': bid_keyword['matchType']}
            keyword_stack.append(filter_data)
        return self.parse_bid_data(profile_id, keyword_stack, adgroup_id)

    def parse_bid_data(self, profile_id, keyword_stack, adgroup_id):
        parse_bidding_content = {}
        parse_keyword_list = []
        parse_bidding_content['adGroupId'] = adgroup_id
        keyword_match_type_list = []
        while len(keyword_stack) != 0:
            if len(keyword_match_type_list) != 100:
                keyword_match_type_list.append(keyword_stack.pop())
            else:
                structured_request = {'adGroupId': adgroup_id, 'keywords': keyword_match_type_list}
                get_bid_recommendation = self.get_bid_recommendation(structured_request, profile_id)
                for bid_recommendation in get_bid_recommendation:
                    parse_keyword_list.append(bid_recommendation)
                keyword_match_type_list = []
        if len(keyword_match_type_list) != 0:
            structured_request = {'adGroupId': adgroup_id, 'keywords': keyword_match_type_list}
            get_bid_recommendation = self.get_bid_recommendation(structured_request, profile_id)
            for bid_recommendation in get_bid_recommendation:
                parse_keyword_list.append(bid_recommendation)
        parse_bidding_content['keywords'] = parse_keyword_list
        return parse_bidding_content

    def get_bid_recommendation(self, structured_request, profile_id):
        get_bid_recommendation_url = 'https://advertising-api-eu.amazon.com/v2/sp/keywords/bidRecommendations'
        bid_recommendation_response = self.auth.ams_post(get_bid_recommendation_url, profile_id, structured_request)
        return bid_recommendation_response.json()['recommendations']
