import gzip

import requests
from json import loads
import authorization.authorizationservice as utils


class ReportService:
    utilities = utils.AuthorizationService()

    def generate_report_id(self, record_type, profile_id, report_date, tactic):
        report_id_url = 'https://advertising-api-eu.amazon.com/v2/sd/' + record_type + '/report'
        request_body = {
            'reportDate': report_date
        }
        if record_type == 'campaigns' and tactic == 'T00001':
            request_body['tactic'] = 'T00001'
            request_body[
                'metrics'] = 'campaignName,campaignId,campaignStatus,currency,impressions,clicks,cost,attributedDPV14d,attributedUnitsSold14d,attributedSales14d'
        if record_type is None and tactic == 'remarketing':
            request_body['tactic'] = 'remarketing'
            request_body[
                'metrics'] = 'campaignName,campaignId,impressions,clicks,clicks,currency,attributedConversions1d,attributedConversions7d,attributedConversions14d,attributedConversions30d,attributedConversions1dSameSKU,attributedConversions7dSameSKU,attributedConversions14dSameSKU,attributedConversions30dSameSKU,attributedUnitsOrdered1d,attributedUnitsOrdered7d,attributedUnitsOrdered14d,attributedUnitsOrdered30d,attributedSales1d,attributedSales7d,attributedSales14d,attributedSales30d,attributedSales1dSameSKU,attributedSales7dSameSKU,attributedSales14dSameSKU,attributedSales30dSameSKU'
        if record_type == 'adGroups' and tactic == 'remarketing':
            request_body['tactic'] = 'remarketing'
            request_body[
                'metrics'] = 'adGroupName,adGroupId,campaignName,campaignId,impressions,clicks,clicks,currency,attributedConversions1d,attributedConversions7d,attributedConversions14d,attributedConversions30d,attributedConversions1dSameSKU,attributedConversions7dSameSKU,attributedConversions14dSameSKU,attributedConversions30dSameSKU,attributedUnitsOrdered1d,attributedUnitsOrdered7d,attributedUnitsOrdered14d,attributedUnitsOrdered30d,attributedSales1d,attributedSales7d,attributedSales14d,attributedSales30d,attributedSales1dSameSKU,attributedSales7dSameSKU,attributedSales14dSameSKU,attributedSales30dSameSKU'
        if record_type == 'productAds' and tactic == 'remarketing':
            request_body['tactic'] = 'remarketing'
            request_body[
                'metrics'] = 'adGroupName,adGroupId,asin,sku,campaignName,campaignId,impressions,clicks,clicks,currency,attributedConversions1d,attributedConversions7d,attributedConversions14d,attributedConversions30d,attributedConversions1dSameSKU,attributedConversions7dSameSKU,attributedConversions14dSameSKU,attributedConversions30dSameSKU,attributedUnitsOrdered1d,attributedUnitsOrdered7d,attributedUnitsOrdered14d,attributedUnitsOrdered30d,attributedSales1d,attributedSales7d,attributedSales14d,attributedSales30d,attributedSales1dSameSKU,attributedSales7dSameSKU,attributedSales14dSameSKU,attributedSales30dSameSKU'

        if 'metrics' not in request_body.keys():
            return 'Invalid Request(Provide correct record type)'

        report_id = self.utilities.ams_post(report_id_url, profile_id, request_body)
        return report_id.json()['reportId']

    def get_report_link(self, profile_id, report_id):
        report_link_url = 'https://advertising-api-eu.amazon.com/v2/reports/' + report_id
        report_link = self.utilities.ams_get(report_link_url, profile_id)
        return report_link

    def get_report_data(self, profile_id, location):
        access_token = self.utilities.generate_access_token()
        requests_data = {
            'Amazon-Advertising-API-Scope': profile_id,
            'Authorization': 'Bearer ' + access_token,
            'Content-Type': 'application/json',
            'Amazon-Advertising-API-ClientId': 'amzn1.application-oa2-client.a7d9e7fefb6a406eaf15115bc98c18f5',
        }
        response = requests.get(location, headers=requests_data, allow_redirects=True)
        json_response = loads(gzip.decompress(requests.get(response.url).content))
        return json_response
