import authorization.authorizationservice as util


class CampaignService:
    utilities = util.AuthorizationService()

    def list_campaigns(self, profile_id):
        list_campaign_url = 'https://advertising-api-eu.amazon.com/sd/campaigns'
        return self.utilities.ams_get(list_campaign_url, profile_id)

    def get_campaigns(self, profile_id, campaign_id):
        campaign_get_url = 'https://advertising-api-eu.amazon.com/sd/campaigns' + campaign_id
        return self.utilities.ams_get(campaign_get_url, profile_id)

    def get_campaigns_extended(self, profile_id, campaign_id):
        campaign_extended_url = 'https://advertising-api-eu.amazon.com/sd/campaigns/extended' + campaign_id
        return self.utilities.ams_get(campaign_extended_url, profile_id)
