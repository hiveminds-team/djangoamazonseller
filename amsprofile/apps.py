from django.apps import AppConfig


class AmsprofileConfig(AppConfig):
    name = 'amsprofile'
