import authorization.authorizationservice as util


class ProfileService:

    utilities = util.AuthorizationService()

    def get_all_profiles(self):
        profile_url = 'https://advertising-api-eu.amazon.com/v2/profiles'
        profile_list = self.utilities.ams_get(profile_url, None)
        return profile_list
