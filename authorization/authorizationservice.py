import json

import requests


class AuthorizationService:

    def ams_get(self, complete_url, profile_id):
        access_token = self.generate_access_token()
        requests_data = {
            'Amazon-Advertising-API-Scope': profile_id,
            'Authorization': 'Bearer ' + access_token,
            'Content-Type': 'application/json',
            'Amazon-Advertising-API-ClientId': 'amzn1.application-oa2-client.a7d9e7fefb6a406eaf15115bc98c18f5',
        }
        ams_api_get_response = requests.get(complete_url, headers=requests_data)
        return ams_api_get_response.json()

    def ams_post(self, complete_url, profile_id, request_body):
        access_token = self.generate_access_token()
        requests_data = {
            'Amazon-Advertising-API-Scope': profile_id,
            'Authorization': 'Bearer ' + access_token,
            'Content-Type': 'application/json',
            'Amazon-Advertising-API-ClientId': 'amzn1.application-oa2-client.a7d9e7fefb6a406eaf15115bc98c18f5',
        }
        ams_post_request = requests.post(complete_url, data=json.dumps(request_body), headers=requests_data)
        return ams_post_request

    @staticmethod
    def generate_access_token():
        url = 'https://api.amazon.com/auth/o2/token'
        authorization_data = {
            'grant_type': 'refresh_token',
            'refresh_token': 'Atzr|IwEBIGgU9oYbAxmbfJylcrho7nDDWFoWl20OhoIlx50txLpBGX73uYZAscB3_40C6X5Y3egaLne5w6-KSzN0ckXZCyTJ3wOHIjS8muglM9zwGmH9SAt6seKoTGibI1nHxengJvWpFphCdr_0JebxfaK1GRHXlCQSOJT6xdyA_K5wBRJU0znSqWdgEwYlHnD6Qv_bg4vL8K7cpsy858tXP3IZXt5iba2U5C5LiLZ1aJ-IIZuFqwxb7_BEOAEnI7Ft0XUKmgFh8ZXKMV1UYg4cmGyIW6tmj3-pYPAkRBJq1idn1EDANoYgqhg8fhlneRKZfeH8-B2HwDxGIFIJNpX8OoHpmpwPz0NToHt8-u9cmW9OOaKGSXIqLMEX3a-iNoZrwsETgrIC9C22PXoVD5SE65rNtTD2jtJ9jUMlTwwi8hRxeaiMlwoL2bMWd5zwgHNOvqE0GzE0VNGnFIsUaAI_1ylsQoQdcJJaJqLbYzjvQ1VVSciy_DSG3cbk0wnXhQGEid7rreGk4RE--fMlaG_Nucg0bfIIiUqj7oZb1kWjWG3gPJWVqvluPJLSEUxeT-y_8Yfczrw9U3GCoFFcRudNK3T9phc2WwUJuIiUEqY4PQrm4o7uWD3w3vIP6WbxUds5kH3kJmGXiF5-kz6t2RMw15RnW9Gv',
            'client_id': 'amzn1.application-oa2-client.a7d9e7fefb6a406eaf15115bc98c18f5',
            'client_secret': '984e8e1f7c519ecb8de742cae4aebae98680fd1974695dfad6db5e99b16c89ce'
        }
        response = requests.post(url, data=authorization_data)
        access_token = response.json()['access_token']
        return access_token
