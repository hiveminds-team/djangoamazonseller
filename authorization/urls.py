from django.urls import path,include

import authorization.views as views
urlpatterns = [
    path('profiles/', views.get_profiles),
    path('report/', views.get_report),
    path('campaign/', views.get_campaigns),
    path('bid/', views.bid_integrate)
]
