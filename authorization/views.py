import json

import requests
from django.http import HttpResponse
import amsprofile.profileservice as util
import sponsoredproduct.reporthelper as repohelper
import integration.reportintegration as report_integration
import integration.bidintegration as bid_integration


# Create your views here.

def get_profiles(request):
    utilities = util.ProfileService()
    return HttpResponse(utilities.get_all_profiles())


def get_report(request):
    rep = repohelper.ReportHelper()
    return HttpResponse(rep.generate_sp_report())


def get_campaigns(request):
    sp = report_integration.ReportIntegration()
    sp.csv_report()
    return HttpResponse('OK')


def bid_integrate(requests):
    bid_util = bid_integration.BidIntegration()
    return HttpResponse(bid_util.get_all_bidding_data_by_profile('3357819201937200'))
